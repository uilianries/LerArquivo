/**
 * @file ProcessadorArquivoTest.java
 * Valida processamento de um arquivo
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.arquivo;

import org.junit.Test;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.TreeMap;

import static org.junit.Assert.assertEquals;

/**
 * Created by uilian on 8/30/16.
 */
public class ProcessaArquivoTest {

    private static final Path PATH_PREFIX = FileSystems.getDefault().getPath("src", "test", "conf");

    /**
     * Valida a abertura de um arquivo vazio
     */
    @Test(expected=ArquivoVazioException.class)
    public void ArquivoVazioTest() throws ArquivoVazioException, LeituraArquivoException, FormatoInvalidoException {
        Path arquivo = PATH_PREFIX.resolve("vazio.txt");
        ProcessadorArquivo.processar(arquivo);
    }

    /**
     * Valida a abertura de um arquivo inexistente
     */
    @Test(expected=LeituraArquivoException.class)
    public void LeituraArquivoTest() throws ArquivoVazioException, LeituraArquivoException, FormatoInvalidoException {
        Path arquivo = PATH_PREFIX.resolve("foobar.txt");
        ProcessadorArquivo.processar(arquivo);
    }

    /**
     * Valida a abertura de um arquivo com formato invalido
     */
    @Test(expected=FormatoInvalidoException.class)
    public void FormatoInvalidoTest() throws ArquivoVazioException, LeituraArquivoException, FormatoInvalidoException {
        Path arquivo = PATH_PREFIX.resolve("formato-invalido.txt");
        ProcessadorArquivo.processar(arquivo);
    }

    /**
     * Valida a abertura de um arquivo valido
     */
    @Test
    public void FormatoValidoTest() throws ArquivoVazioException, LeituraArquivoException, FormatoInvalidoException {
        Path arquivo = PATH_PREFIX.resolve("formato-valido.txt");
        TreeMap<String, String> mapa = ProcessadorArquivo.processar(arquivo);
        assertEquals(3, mapa.size());
    }
}

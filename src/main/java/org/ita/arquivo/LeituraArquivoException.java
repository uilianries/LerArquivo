/**
 * @file LeituraArquivoException.java
 * Implementação de exceção
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.arquivo;

import java.nio.file.Path;

/**
 * Lança exceção para erro na leitura
 */
public class LeituraArquivoException extends Exception {

    /**
     * Cria exceção de arquivo
     * @param arquivo Caminho do arquivo
     */
    public LeituraArquivoException(final Path arquivo) {
        super("Erro ao abrir o arquivo " + arquivo);
    }
}

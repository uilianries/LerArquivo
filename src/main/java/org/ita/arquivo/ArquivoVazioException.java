/**
 * @file ArquivoVazioException.java
 * Implementação de exceção
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.arquivo;

import java.nio.file.Path;

/**
 * Lança exceção para arquivo vazio
 */
public class ArquivoVazioException extends Exception {

    /**
     * Cria exceção de arquivo
     * @param arquivo Caminho do arquivo
     */
    public ArquivoVazioException(final Path arquivo) {
        super("Conteudo vazio no arquivo " + arquivo);
    }
}

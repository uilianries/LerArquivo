/**
 * @file FormatoInvalidoException.java
 * Implementação de exceção
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.arquivo;

import java.nio.file.Path;

/**
 * Lança exceção para linha inavlida
 */
public class FormatoInvalidoException extends Exception {

    /**
     * Cria exceção de arquivo
     * @param arquivo Caminho do arquivo
     */
    public FormatoInvalidoException(final Path arquivo) {
        super("\"Formato de arquivo inválido " + arquivo);
    }
}

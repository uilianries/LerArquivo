/**
 * @file ProcessadorArquivo.java
 * Realiza processamento de um arquivo
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.arquivo;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Abre um arquivo e processa dados internos
 */
public class ProcessadorArquivo {

    /**
     * Processa arquivo com estrutura chave->valor
     * @param arquivo  Caminho do arquivo
     */
    public static TreeMap<String, String> processar(final Path arquivo) throws LeituraArquivoException, ArquivoVazioException, FormatoInvalidoException {
        if (!validarArquivo(arquivo)) {
            throw new LeituraArquivoException(arquivo);
        }

        if (isArquivoVazio(arquivo)) {
            throw new ArquivoVazioException(arquivo);
        }

        if (!validarConteudoArquivo(arquivo)) {
            throw new FormatoInvalidoException(arquivo);
        }

        return extrairPares(arquivo);
    }

    /**
     * Valida existencia e permissão de arquivo
     * @param arquivo Caminho do arquivo
     * @return Se o arquivo existe
     */
    private static boolean validarArquivo(final Path arquivo) {
        return Files.exists(arquivo) && Files.isRegularFile(arquivo);
    }

    /**
     * Valida se o conteudo do arquivo é vazio
     * @param arquivo Caminho do arquivo
     * @return True se o arquivo é vazio
     */
    private static boolean isArquivoVazio(final Path arquivo) {
        File file = new File(arquivo.toString());
        return file.length() == 0;
    }

    /**
     * Valida se string está no formato chave->valor
     * @param linha Linha do arquivo
     * @return True se está correta
     */
    private static boolean validarLinha(final String linha) {
        Pattern regex = Pattern.compile("\\w+->\\w+");
        Matcher matcher = regex.matcher(linha);
        return matcher.matches();
    }

    /**
     * Carrega arquivo para um buffer
     * @param arquivo caminho do arquito
     * @return Lista com todas as linha do arquivo
     */
    private static List<String> carregarArquivo(final Path arquivo) throws LeituraArquivoException {
        List<String> linhas;
        try {
            BufferedReader buffered = Files.newBufferedReader(arquivo);
            linhas = buffered.lines().collect(Collectors.toList());
        } catch (IOException e) {
            throw new LeituraArquivoException(arquivo);
        }
        return linhas;
    }

    /**
     * Valida o conteúdo do arquivo
     * @param arquivo Arquivo que deve ser validado
     * @return true, se o arquivo é valido
     */
    private static boolean validarConteudoArquivo(final Path arquivo) throws LeituraArquivoException {
        List<String> linhas = carregarArquivo(arquivo);
        for (String linha : linhas) {
            if (!validarLinha(linha)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Extrai conteúdo de linhas e transforma em pares
     * @param arquivo Arquivo para ser tratado
     * @return Linhas tratadas em pares
     * @throws LeituraArquivoException
     */
    private static TreeMap<String, String> extrairPares(final Path arquivo) throws LeituraArquivoException {
        List<String> linhas = carregarArquivo(arquivo);
        Pattern regex = Pattern.compile("(\\w+)->(\\w+)");
        TreeMap<String, String> mapa = new TreeMap<>();
        for (final String linha : linhas) {
            Matcher matcher = regex.matcher(linha);
            if (!matcher.find()) {
                throw new LeituraArquivoException(arquivo);
            }
            mapa.put(matcher.group(1), matcher.group(2));
        }
        return mapa;
    }

}
